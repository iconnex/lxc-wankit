docker-wan-emulator
===================

## h.2 Install ##
=======

```
#!bash

apt-get install docker.io

docker build git@bitbucket.org:iconnex/lxc-wankit.git#stable

route add default gw 192.168.99.1 dev tun0

```

### Overview ###

The netem parameters are set as follows:

```
uci add netem interface
uci set netem.@interface[0].ifname=eth0
uci set netem.@interface[0].enabled=1
uci set netem.@interface[0].delay=1
uci set netem.@interface[0].delay_ms=151
```